import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Movies from "./component/Movies";
import Header from "./component/Header";
// import CarouselPage from './component/CarouselPage';
// import MovieList from './component/MovieList';
// import MovieVideo from './component/MovieVideo';
import MovieDetail from "./component/MovieDetail";
import Now_playing from "./component/Now_playing";
// import Popular from './component/Popular';
import Top_Rate from "./component/Top_Rate";
import Upcoming from "./component/Upcoming";
import Login from "./component/Login";
import Signup from "./component/Signup";
import ShowHeader from "./component/ShowHeader";
function App() {
  return (
    <div>
      <Router>
        {/* <CarouselPage/> */}
        <ShowHeader>
          <Header/>
        </ShowHeader>
        <Routes >
            <Route path="/" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/movie" element={<Movies />} />
            <Route path="/movieDetail" element={<MovieDetail />} />
            {/* <Route path='/popular' element={<Popular/>}/> */}
            <Route path="/now_playing" element={<Now_playing />} />
            <Route path="/top_rate" element={<Top_Rate />} />
            <Route path="/upcoming" element={<Upcoming />} />
            {/* <Route path="*" element={<h1>Error Page</h1>} /> */}
        </Routes>
      </Router>
    </div>
  );
}

export default App;
