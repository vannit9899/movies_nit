import { Link, useLocation } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Trailer from "../component/Trailer";
const MovieDetail = () => {
  const location = useLocation();
  console.log(location);
  return (
    <div style={{ backdropFilter: "blur(5px)" }}>
      <div
        className="container-fluid pt-xl-5"
        style={{
          backgroundImage: `linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url(https://image.tmdb.org/t/p/original${location.state.movie?.backdrop_path})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "69%",
          backgroundAttachment: "fixed",
          height: "100vh",
        }}
      >
        <div className="row">
          <div className="col-lg-4 col-xs-12 text-center img-fluid">
            <img
              src={`https://image.tmdb.org/t/p/original${location.state.movie?.poster_path}`}
              alt=""
              width={"350px"}
            />
          </div>
          <div
            className="col-lg-8 col-md-12 text-start"
            style={{ width: "50rem" }}
          >
            <h1 className="fw-bold text-white">
              {location.state.movie?.title}
            </h1>
            <p className="text-white d-none d-md-block">
              Popularity:{" "}
              <span className="text-danger">
                {location.state.movie?.popularity}
              </span>{" "}
              | Vote Average:{" "}
              <span className="text-danger">
                {location.state.movie?.vote_average}
              </span>{" "}
              | Vote Count:{" "}
              <span className="text-danger">
                {location.state.movie?.vote_count}
              </span>{" "}
              | Release Date:{" "}
              <span className="text-danger">
                {location.state.movie?.release_date}
              </span>
            </p>
            <span className="text-white d-none d-xl-block ">
              <span className="fw-bold">Overview: </span>
              {location.state.movie?.overview}
            </span>
            <div className="d-flex gap-2 mt-1">
              <Link to="/movie">
                <button className="btn btn-danger">Back</button>
              </Link>
              <Trailer location={location} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieDetail;
