import React from 'react'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css'
import { useNavigate } from 'react-router-dom';
const Header = () => {
  // let user = JSON.parse(localStorage.getItem('user_info'))
  const navigate = useNavigate()
  return (
    <Navbar bg="dark" data-bs-theme="dark" expand="lg" className="bg-body-tertiary sticky-top bg-black">
      <Container fluid>
        <Navbar.Brand href="#">N-I-T Movies</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">

              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: '100px' }}
                navbarScroll
              >
                <Nav.Link href="/movie">Home</Nav.Link>
                {/* <Nav.Link href="/popular">Popular</Nav.Link> */}
                <Nav.Link href="/now_playing">Now Playing</Nav.Link>
                <Nav.Link href="/top_rate">Top Rated</Nav.Link>
                <Nav.Link href="/upcoming">Upoming</Nav.Link>
              </Nav>
              <Form className="d-flex gap-2">
                <Button
                  variant="danger"
                  onClick={() => {
                    localStorage.clear();
                    navigate("/");
                  }}
                >
                  Logout
                </Button>
              </Form>
            </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Header