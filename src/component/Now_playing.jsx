import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
const Now_playing = () => {
    const [nowplaying, setNowplaying] = useState([])
    const getNowplaying = () => {
        fetch (`https://api.themoviedb.org/3/movie/now_playing?api_key=9a9c003c87c5529d40863ec245a50052&language=en-US&page=1`
        )
        .then((res) => res.json()) 
        .then((json) => setNowplaying(json.results))
        // .then((json) => console.log(json.results))
    };
    useEffect(() => {
        getNowplaying()
    },[])
  return (
    <div className="container-fluid mt-5">
      <div className="d-flex flex-wrap justify-content-center gap-4">
        {nowplaying.map((movie) => (
          <div>
            <Link to='/movieDetail' state={{movie:movie}}>
              <div>
                <img
                  className="poster"
                  src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                  alt=""
                />
              </div>
            </Link>
            <div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Title:</span>
                <span className=" text-dark " style={{ width: "10rem" }}>
                  {movie.title}
                </span>
              </div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Popularity:</span>
                <span className="text-dark " style={{ width: "10rem" }}>
                  {movie.popularity}
                </span>
              </div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Release & Date:</span>
                <span className=" text-danger " style={{ width: "10rem" }}>
                  {movie.release_date}
                </span>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Now_playing