import React, { useEffect, useState } from "react";
import "../assets/Movie.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link, useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
const Movies = () => {
  const navigate = useNavigate();
  const [movieList, setMovieList] = useState([]);
  const getMovie = () => {
    fetch(
      `https://api.themoviedb.org/3/discover/movie?api_key=9a9c003c87c5529d40863ec245a50052`
    )
      .then((res) => res.json())
      .then((json) => setMovieList(json.results));
    // .then((json) => console.log(json.results));
  };
  useEffect(() => {
    getMovie();
  }, []);
  console.log(movieList);
  return (
    <div className="container-fluid mt-5">
      <div className="d-flex flex-wrap justify-content-center gap-4">
        {movieList.map((movie) => (
          <div>
            <Link to="/movieDetail" state={{ movie: movie }}>
              <div>
                <img
                  className="poster"
                  src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                  alt=""
                />
              </div>
            </Link>
            <div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Title:</span>
                <span className=" text-dark " style={{ width: "10rem" }}>
                  {movie.title}
                </span>
              </div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Popularity:</span>
                <span className="text-dark " style={{ width: "10rem" }}>
                  {movie.popularity}
                </span>
              </div>
              <div className="d-flex gap-2">
                <span className="fw-bold">Release & Date:</span>
                <span className=" text-danger " style={{ width: "10rem" }}>
                  {movie.release_date}
                </span>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Movies;
