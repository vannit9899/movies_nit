import React from 'react'
import { useState } from 'react'
const Signup = () => {
    const [email,setEmail] =useState("")
    const [password,setPassword] = useState("")
    const [username,setUsername] = useState("")

    function handleSignup(){
      let item = {username,email,password}
      console.warn(item)
    }
  return (
    <div className='App wrapper'>
    <div className='login text-white border border-0 text-center'>
        <h1>Sign Up</h1>
        <div className='input_box'>
            <input type="email" onChange={(e)=>setEmail(e.target.value)} placeholder='Email' /> <br />
            <input type="text" onChange={(e)=>setUsername(e.target.value)}  placeholder='Username'/> <br />
            <input type="password" onChange={(e)=>setPassword(e.target.value)} placeholder='Password'/> <br />
        </div>
        <button onClick={handleSignup}  className='btn btn-primary mt-3'>Sign Up</button>
    </div>
</div>
  )
}

export default Signup