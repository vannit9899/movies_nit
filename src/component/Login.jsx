import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import "../assets/Login.css";
const Login = () => {  
    const navigate = useNavigate()
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');

    const handleLogin = (e) =>{
        axios.post(`https://dummyjson.com/auth/login`,{
            username : username,
            password : password
        })
        .then(result =>{
            localStorage.setItem('token', result.data.token)
            // alert('Success')
            navigate('/movie')
        })
        .catch(error =>{
            alert('service error')
        })
    }

  return (
    <div className='App wrapper'>
            <div className='login text-white border border-0 text-center'>
                <h1>Login</h1>
                <div className='input_box'>
                    <input type="text" onChange={(e)=>setUsername(e.target.value)} placeholder='Username' /> <br />
                    <input type="password" onChange={(e)=>setPassword(e.target.value)} placeholder='Password'/> <br />
                </div>
                <button onClick={handleLogin}  className='btn btn-primary mt-3'>Login</button>
            </div>
    </div>
  )
}

export default Login