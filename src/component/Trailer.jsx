import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Youtube from 'react-youtube';
const Trailer = ({location}) => {
  const [lgShow, setLgShow] = useState(false);
  const [trailer,setTrailer] =  useState([]);
  const showTrailer=()=>{
    fetch(`https://api.themoviedb.org/3/movie/${location.state.movie.id}/videos?api_key=9a9c003c87c5529d40863ec245a50052&language=en-US`)
    .then(res=>res.json())
    .then(json=> setTrailer(json.results))
  }
  useEffect(()=>{
    showTrailer()
  },[])
  return (
    <div>
    <>
      <Button className='btn btn-warming' onClick={() => setLgShow(true)}>Play Trailer</Button>
      <Modal
        size="lg"
        show={lgShow}
        onHide={() => setLgShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
      >
        <Modal.Header closeButton className='bg-danger'>
          <Modal.Title id="example-modal-sizes-title-lg" className='text-light'>
          {location.state.movie?.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='bg-black'>
            <Youtube className='text-center' videoId={trailer[0]?.key} />
        </Modal.Body>
      </Modal>
    </>
    </div>
  )
}

export default Trailer
