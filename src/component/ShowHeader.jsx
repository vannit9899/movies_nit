import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'

const ShowHeader = ({children}) => {
    const location = useLocation();
    const [show,setShow] = useState(false)
    useEffect(()=>{
        // console.log('This is location: ' ,location)
        if(location.pathname === '/' || location.pathname === '/movieDetail'){
            setShow(false)
        }else{
            setShow(true)
        }
    },[location])
  return (
    <div>{show && children}</div>
  )
}

export default ShowHeader